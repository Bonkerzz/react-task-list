const React = require('react');

/**
 * The TaskList component renders a view for a list
 * of tasks.
 */
const TaskList = React.createClass({
  // Display name for the component (useful for debugging)
  displayName: 'TaskList',

  // Describe how to render the component
  render: function() {
    return (
      <div>
        <ul>
          <li>Clean my bed</li>
          <li>Finish my homework</li>
          <li>Brush my teeth</li>
        </ul>
      </div>
    );
  }
});

// Export the TaskList component
module.exports = TaskList;
