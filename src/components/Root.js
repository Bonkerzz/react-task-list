const React = require('react');

// Require our TaskList React component
const TaskList = require('./TaskList');

/**
 * The root React component from which all other components
 * on the page are descended.
 */
const Root = React.createClass({
  // Display name for the component (useful for debugging)
  displayName: 'Root',

  // Describe how to render the component
  render: function() {
    return (
      <TaskList />
    );
  }
});

// Export the Root component
module.exports = Root;
